import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { DestinosViajes } from './../models/destinos-viajes.model';



@Component({
  selector: 'app-destinos-viajes',
  templateUrl: './destinos-viajes.component.html',
  styleUrls: ['./destinos-viajes.component.css']
})
export class DestinosViajesComponent implements OnInit {
  @Input() destino: DestinosViajes;
  @Input("idx") position: number;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() onClicked: EventEmitter<DestinosViajes>;
   
  constructor() {
    this.onClicked = new EventEmitter();
  }

  ngOnInit() {
  }

  ir() {
    this.onClicked.emit(this.destino);
    return false;
  }
}
