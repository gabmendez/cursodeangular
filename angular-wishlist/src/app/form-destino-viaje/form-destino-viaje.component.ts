import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { DestinosViajes } from '../models/destinos-viajes.model';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax, AjaxResponse } from 'rxjs/ajax';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinosViajes>;
  fg: FormGroup;
  minLongitud = 3;
  searchResults: string[];
  
  constructor(public fb: FormBuilder) {
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      name: ['', Validators.compose([
        Validators.required,
        this.nameValidator,
        this.nameValidatorParametrizable(this.minLongitud)
      ])],
       
      url: ['']
    });
    
    this.fg.valueChanges.subscribe((form: any) =>{
      console.log('cambio el formulario: ', form);
    });
   }

  ngOnInit() {
    const elemName = <HTMLInputElement>document.getElementById('name');
    fromEvent(elemName, 'input')
    .pipe (
      map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
      filter(text => text.length > 2),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(() => ajax('/assets/datos.json'))
    ).subscribe( ajaxResponse => {
      console.log(ajaxResponse.response);
      this.searchResults = ajaxResponse.response;
    });
  }

  guardar(name: string, url: string): boolean {
    const d = new DestinosViajes(name, url);
    this.onItemAdded.emit(d);
    return false;
  }

  nameValidator (control: FormControl): {[s: string]: boolean} {
    const l = control.value.toString().trim().length;
    if (l > 0 && l < 5) {
      return { invalidName: true};
    }
    return null;
  }

  nameValidatorParametrizable(minLong: number): ValidatorFn {
    return(control: FormControl): {[s: string]: boolean} | null => {
      const l = control.value.toString().trim().length;
      if (l > 0 && l < minLong) {
        return { minLongName: true};
      }
      return null;
    }
  }
}
