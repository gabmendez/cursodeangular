import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { DestinosViajes } from './../models/destinos-viajes.model';
import { DestinosApiClient } from './../models/destinos-api-client.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
})
export class ListaDestinosComponent implements OnInit { 
@Output() onItemAdded: EventEmitter<DestinosViajes>;
updates: string[];

  
  constructor (public destinosApiClient: DestinosApiClient) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.destinosApiClient.subscribeOnChange((d: DestinosViajes) => {
      if(d ! = null) {
        this.updates.push('se ha elegido a ' + d.name);
      }
    })
  
  }

   
  ngOnInit() {
 
  }

  agregado(d: DestinosViajes) {
  this.destinosApiClient.add(d);
  this.onItemAdded.emit(d);
  }
  elegido(e: DestinosViajes) {
    this.destinosApiClient.elegir(e);
  }
}
