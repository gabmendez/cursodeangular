import {v4 as uuid} from 'uuid';

export class DestinosViajes {
  selected: boolean;
  servicios: string[];
  id = uuid();

  constructor(public name: string, public url: string) {
      this.servicios = ['pileta', 'desayuno'];
   }
   
   isSelected(): boolean{
    //console.log(this.selected);
    return this.selected;
  }
    
   setSelected(s: boolean) {
    this.selected = s;
  }

  
}