import { DestinosViajes } from './destinos-viajes.model';
import { Subject, BehaviorSubject } from 'rxjs';

export class DestinosApiClient {
	destinos: DestinosViajes[];
    current: Subject<DestinosViajes> = new BehaviorSubject<DestinosViajes>(null);

	constructor() {
       this.destinos = [];
	}
	add(d: DestinosViajes) {
	  this.destinos.push(d);
	}
	getAll() {
	  return this.destinos;
	}
	getById(id: String):DestinosViajes {
		return this.destinos.filter(function(d) {return d.id.toString() === id; })[0];
	}
	elegir(d: DestinosViajes) {
		this.destinos.forEach(x => x.setSelected(false));
		d.setSelected(true);
		this.current.next(d);
	}
	subscribeOnChange(fn) {
		this.current.subscribe(fn);
	}
}